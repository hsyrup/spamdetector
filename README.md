# DATA ANALYTICS
## Spam Mail Detector                                                             

>Apr.-May. 2015  
>School of Mathematics, Fudan University                                              
>Supervisor: P. Wenlian Lu  

* Built a java program to detect whether a mail is spam or not, with the method of Naïve Bayes. Tested on UCI Spambase Data Set dataset. Accuracy showed by doing Pearson Test.
* Developed a GUI by Java for the Spam Detector, being functional of classifying specific mails and updating the mail library behind.
