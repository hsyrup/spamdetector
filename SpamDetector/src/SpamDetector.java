import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.*;
import javax.swing.border.Border;


class Library{
	Map<String,Integer> kmap=new HashMap<String,Integer>();
	int mapSize;
	int standard;
	double spamratio;
	double[][] library;//0:prob in ham   1:prob in spam    2:prob in total
	
	@SuppressWarnings("resource")
	Library() throws IOException{
		BufferedReader reader=new BufferedReader(new FileReader("./library.txt"));
		standard=Integer.valueOf(reader.readLine());
		reader.readLine();		
		mapSize=Integer.valueOf(reader.readLine());//the second line:mapSize
		spamratio=Double.valueOf(reader.readLine());//the third line:ratio
		library=new double[3][mapSize];
	}
	
	@SuppressWarnings("resource")
	public void setMap() throws IOException{
		File librecord=new File("./library.txt");
		BufferedReader reader=new BufferedReader(new FileReader(librecord));
		reader.readLine();
		reader.readLine();
		reader.readLine();
		reader.readLine();
		String line=null;
		int j=0;
		while((line=reader.readLine())!=null){
			String[] data=line.split(" ");			
			kmap.put(data[0], j);
			for(int i=1;i<4;i++){
				library[i-1][j]=Double.valueOf(data[i]);
//				System.out.println(library[i-1][j]);
			}
			j++;
		}
		getMail.getMapLength(kmap.size());
//		System.out.println(kmap.size());
	}
};

class getMail{
	static int mapLength;
	int[] specificMail=new int[mapLength];
	
	public static void getMapLength(int a){mapLength=a;}; 
	
	public void getMailRecord(File file,Map<String,Integer> kwMap2){
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = reader.readLine()) != null) {
//				String line_word = line.replaceAll("[^a-zA-Z]", " "); 
				String line_word = line.replaceAll("	", " ");
				String[] data = line_word.split(" "); 
				aloop: for (String string : data) { 
					String word = string.trim().toLowerCase();
					if (word.equals(""))
						continue aloop;
					int lenght=word.length();
					char character=word.charAt(lenght-1);
			    	while((int)character>122||(int)character<97){
			    		lenght--;
			    		if(lenght>0){
			    			character=word.charAt(lenght-1);
			    			}
			    		else continue aloop;
			    		}				
					word=word.substring(0,lenght);
					if (word.equals(""))
						continue aloop;
					int j=0;
					char character1=word.charAt(j);
					while((int)character1>122||(int)character1<97){
						j++;
		    			if(j<word.length()){
		    				character=word.charAt(j);
		    				}
		    			else continue aloop;
		    			}
					if(kwMap2.get(word)!=null){
						specificMail[kwMap2.get(word)]=1;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

public class SpamDetector implements ActionListener{
	JFrame frame=new JFrame("Spam Detector");
	JPanel p1=new JPanel();
	JTextField tf=new JTextField(20);
	JButton browse=new JButton("Browse");
	JButton btest=new JButton("test");
	JButton bupdate=new JButton("update");
	JPanel p2=new JPanel();
	JPanel p3=new JPanel();
	JLabel l1=new JLabel("这封邮件是：待检测 ");
	JLabel l2=new JLabel();
	JLabel l3=new JLabel("输入待检测的邮件：");
	Library library;
	int tag=0;
	double lsh=1,lhs=5;

	
	
	
	
	SpamDetector(){
		frame.setLayout(new GridLayout(3,1,7,7));
		frame.add(p1);
		p1.add(l3);
		p1.add(tf);
		p1.add(browse);
		Border border1=BorderFactory.createTitledBorder("input mails:");
		p1.setBorder(border1);
		
		frame.add(p2);
		p2.setPreferredSize(null);
		p2.setLayout(new BorderLayout());
		File temp=new File("./library.txt");
		if(temp.exists()==false){
			l2.setText("no current library,please update!");
		}
		p2.add(l2);
		p2.add(bupdate,BorderLayout.EAST);
		Border border2=BorderFactory.createTitledBorder("library_1:");
		p2.setBorder(border2);
		
		frame.add(p3);
		p2.setLayout(new FlowLayout());
		p3.add(l1);
		p3.add(btest);
		Border border3=BorderFactory.createTitledBorder("Model_1:");
		p3.setBorder(border3);
		
		browse.addActionListener(this);
		btest.addActionListener(this);
		bupdate.addActionListener(this);
		
		frame.pack();
		frame.setVisible(true);
		
		{try{		
			if(!l2.getText().equals("no current library,please update!")){					
				library=new Library();
				@SuppressWarnings("resource")
				BufferedReader reader=new BufferedReader(new FileReader("./library.txt"));
				reader.readLine();
				l2.setText("last update at:     "+reader.readLine());
				library.setMap();			
			}		
			}
		catch (IOException e) {e.printStackTrace();}}
		
	}
	
	
	public void actionPerformed(ActionEvent e){
		File file=null;
		JButton button=(JButton)e.getSource();
		
		if(button==browse){
			JFileChooser fc=new JFileChooser();  
			int r=fc.showOpenDialog(frame);  
			if(r==JFileChooser.APPROVE_OPTION)  
			{  
				file=fc.getSelectedFile();  
				tf.setText(file.getAbsolutePath());
				this.tag=1;
//				try{ editor.read(new FileReader(file),null);}  
//				catch(IOException e){}  
		    }
			else{tf.setText("not found!!!");}
		}
		
		if(button==btest){
			if(tag==0){l1.setText("no selected mail");}
			else{
				getMail newMail=new getMail();
				newMail.getMailRecord(new File(tf.getText()).getAbsoluteFile(),library.kmap);
				getMail.mapLength=library.mapSize;
				double score=1;int order=0;
//				double px=1;//int pxo=0;
				for(int j=0;j<getMail.mapLength;j++){
					if(newMail.specificMail[j]==1){score*=(double)library.library[1][j]*(double)library.spamratio/(double)library.library[2][j];}
					else{score*=(1-(double)library.library[1][j]*(double)library.spamratio/(double)library.library[2][j]);}
					while(score<0.83333){score*=1.2;order++;}
				}
				if(order<library.standard){
					l1.setText("这是垃圾邮件");
				}else{
					l1.setText("这是普通邮件");
				}
				
			}			
		}
		
		if(button==bupdate){
			l2.setText("updating...");
			int numberbound=5;
			double upratio=0.8;
//			double downratio=0;
			
			int wordorder=0;
			int[][] wordrecord=new int[3][100000];
			int[][] wordrecord2=new int[3][100000];

			
			Map<String, Integer> countMap = new HashMap<String, Integer>(); 
			updateWordsrecord update = new updateWordsrecord();
			update.init(wordrecord[0],wordrecord2[0],countMap);
	//to collect presence of words in mails
			update.zero();
			for(int i=0;i<3;i++){
				for(int j=0;j<100000;j++){
					wordrecord[i][j]=0;
					wordrecord2[i][j]=0;
				}
			}
			countMap = new HashMap<String, Integer>();
			System.gc();
			update.init(wordrecord[0],wordrecord2[0],countMap);
			for(int select=1;select<11;select++){
				update.undateRecord("./library/ham"+select);
			}
			wordrecord[0]=update.getRecord();
//			wordrecord2[0]=update.getRecord2();
			countMap=update.getMap();
			
			int num1=update.gettotalnum();
			update.init(wordrecord[1],wordrecord2[1],countMap);
			int fold=(int)(Math.random()*10.0)+1;
			for(int select=1;select<11;select++){
				if(select==fold){continue;}
				update.undateRecord("./library/spam"+select);
			}	
			wordrecord[1]=update.getRecord();
//			wordrecord2[1]=update.getRecord2();
			wordorder=update.updateorder();
			int num1_2=update.gettotalnum();
//			int words=update.totalwords;

			
	//pick up words which appear equal to or more than 5 times		
			int[][] keywords=new int[3][wordorder];
			Map<String, Integer> kwMap = new HashMap<String, Integer>(); 
			
				Iterator<String> iter = countMap.keySet().iterator();
				int firstselection=0;
				while (iter.hasNext()) { //
					String key = iter.next();
					int value = countMap.get(key);
					if((wordrecord[0][value]+wordrecord[0][value])>=numberbound){
						kwMap.put(key,firstselection);
						keywords[0][firstselection]=wordrecord[0][value];
						keywords[1][firstselection]=wordrecord[1][value];
						firstselection++;
					}
				}
			wordrecord=null;
			wordrecord2=null;


			
	//calculate ratio		
			float[] ratio1=new float[firstselection];
			for(int i=0;i<firstselection;i++){
				ratio1[i]=(float) (1.0*keywords[1][i]/(keywords[0][i]+keywords[1][i]));
			}


			
	//pick up distinguish ones	
			int[][] keywords2=new int[3][firstselection];
			Map<String, Integer> kwMap2 = new HashMap<String, Integer>();
			
			Iterator<String> iter2 = kwMap.keySet().iterator();
			int selection2=0;
			
			while (iter2.hasNext()) { 
				String key = iter2.next();
				int value = kwMap.get(key);			
				if(ratio1[value]>=upratio){
					kwMap2.put(key,selection2);
					keywords2[0][selection2]=keywords[0][value];
					keywords2[1][selection2]=keywords[1][value];
					keywords2[2][selection2]=keywords[2][value];
					selection2++;
				}
			}
			keywords=null;
//			System.out.println(kwMap2.size());		
			
			
			

	//calculate probability
			double[][] library=new double[3][selection2];
			double pspam=1-(double)num1/num1_2;
			
			for(int i=0;i<selection2;i++){			
				library[0][i]=((double)keywords2[0][i]+1)/(num1_2-num1+2);
				library[1][i]=((double)keywords2[1][i]+1)/(num1_2-num1+2);				
				library[2][i]=(double)(keywords2[0][i]+keywords2[1][i]+2)/(num1_2+2);
			}
			
			Map<Integer,String> newmap = new HashMap<Integer,String>();
			Iterator<String> it = kwMap2.keySet().iterator();
			while(it.hasNext()){
				String key=it.next();
				int value=kwMap2.get(key);
				newmap.put(value,key);
			}
			
			int[][] information0=new int[selection2][1000];
			int[][] information1=new int[selection2][1000];
			getV getV1=new getV(),getV2=new getV();
			
			information0=getV1.get(0,"./library/ham"+fold, kwMap2);
			information1=getV2.get(1,"./library/spam"+fold, kwMap2);
			
			int hnum=getV1.getmailnum();
			int snum=getV2.getmailnum();
			
			double[] score=new double[hnum];
			int[] nums1=new int[hnum];
			double ratio11=0.0;
			int[] order1=new int[hnum];
//			double[] px=new double[hnum];
//			int[] pxo=new int[hnum];
			
			double[] score2=new double[snum];
			int[] nums2=new int[snum];
			double ratio22=0.0;
			int[] order2=new int[snum];
//			double[] px2=new double[snum];
//			int[] pxo2=new int[snum];
			
			
			
			for(int i=0;i<hnum;i++){
				score[i]=1;order1[i]=0;nums1[i]=0;
				for(int j=0;j<selection2;j++){
					double temp=library[1][j]*pspam/library[2][j];
					if(information0[j][i]==1){score[i]*=temp;nums1[i]++;}
					else{score[i]*=(double)(1-temp);}
					while(score[i]<0.83333){score[i]*=1.2;order1[i]++;}
				}
			}
			for(int i=0;i<snum;i++){
				score2[i]=1;order2[i]=0;nums2[i]=0;
				for(int j=0;j<selection2;j++){
					double temp=library[1][j]*pspam/library[2][j];
					if(information1[j][i]==1){score2[i]*=temp;nums2[i]++;}
					else{score2[i]*=(1-temp);}
					while(score2[i]<0.83333){score2[i]*=1.2;order2[i]++;}
				}
			}
			
			int standard=0;
			double minimax=0.0;
			while(minimax<=1.0){ratio11=0;ratio22=0;
				for(int i=0;i<hnum;i++){
					if(order1[i]<standard){ratio11++;}
				}	
				for(int i=0;i<snum;i++){
					if(order2[i]<standard){ratio22++;}
				}
				minimax=(ratio22/snum+(1-pspam)*lhs/pspam/lsh*ratio11/hnum);
				System.out.println(minimax);
				standard++;
			}
			standard-=2;
			System.out.println(standard);
			
			File lib=new File("./library.txt");
			BufferedWriter output;
			Date date=new Date();
			String time=date.toString();
			try {
				if(!lib.exists()){lib.createNewFile();}					
				output = new BufferedWriter(new FileWriter(lib));
				output.write(Integer.toString(standard));
				output.newLine();
				output.write(time);
				output.newLine();
				output.write(Integer.toString(newmap.size()));
				output.newLine();
				output.write(Double.toString(pspam));
				output.newLine();
				for(int i=0;i<newmap.size();i++){
					output.write(newmap.get(i));
					for(int j=0;j<3;j++){	
						output.write(" ");
						output.write(Double.toString(library[j][i]));
					}
						output.newLine();
				}
				output.flush();
				output.close();
				
				
								
				l2.setText("updated at"+time);
			} catch (IOException e1) {
				e1.printStackTrace();
			}			
		}
	}
	
	public static void main(String arg[]){
		@SuppressWarnings("unused")
		SpamDetector sd=new SpamDetector();
	}
	
}
